'''
Created on Mar 14, 2018

@author: alan@thinkskink.com
'''

import os


#Read an input file of user.tablenames

#put it in memory as this will be from the db eventually
tableList=[]
schemaList=[]
tablespaceList=[]
tablespaceQuotaScript=[]

tsdbxHome='/home/alan/eclipse-workspace/tsdbx'
outDir=os.path.join(tsdbxHome,'tsdbxOutput')

tableFile=os.path.join(tsdbxHome,'resource','tsdbx','tableFile.txt')
tablespaceFile=os.path.join(tsdbxHome,'resource','tsdbx','tablespaces.txt')
expParfilesDir=os.path.join(outDir,'parfiles','exp')
impParfilesDir=os.path.join(outDir,'parfiles','imp')
schemaParfilesDir=os.path.join(outDir,'schema_parfiles')
scriptsDir=os.path.join(outDir,'scripts') 
tablespaceQuotaScriptFile=os.path.join(scriptsDir,'tablespaceQuota.sh')

with open(tableFile) as f:
    for line in f :
        tableList.append(line.rstrip().split('.'))
        
for line in tableList :
    tableOwner=line[0]
    tableName=line[1]
    parText ='''
USERID="DBA_TS/dbats@RESBSCS"
BUFFER=300000
FILE="./dumpfiles/tab_''' + tableOwner + '.'+ tableName + '''.dmp"
TABLES='''+ tableOwner + '''.''' + tableName + '''
ROWS=YES
COMPRESS=N
TRIGGERS=N
CONSTRAINTS=N
STATISTICS=NONE
QUERY="WHERE 1=1"
LOG="./log_exp/exp_''' + tableName + '''.log"
'''
    ownerDir=os.path.join(expParfilesDir,tableOwner)
    if not os.path.isdir(ownerDir) :
        os.makedirs(ownerDir)
        
    parfile=os.path.join(ownerDir,'exp_'+ tableName + '.par')
    
    with open(parfile, 'w') as f :
        f.write(parText)
        print "Writing " + parfile
        
for line in tableList :
    tableOwner=line[0]
    tableName=line[1]
    parText ='''
USERID=DBA_TS/dbats@UBSCS_LM
BUFFER=300000
FILE=./dumpfiles/tab_''' + tableOwner + '.' + tableName + '''.dmp
FROMUSER='''+tableOwner + '''
TOUSER='''+tableOwner + '''
ROWS=YES
IGNORE=Y
STATISTICS=NONE
LOG=./log_imp/imp_''' + tableOwner + '.' + tableName + '''.log

'''
    ownerDir=os.path.join(impParfilesDir,tableOwner)
    if not os.path.isdir(ownerDir) :
        os.makedirs(ownerDir)
        
    parfile=os.path.join(ownerDir,'imp_'+ tableName + '.par')
    
    with open(parfile, 'w') as f :
        f.write(parText)
        print "Writing " + parfile
        
#The directory structure is now a list of schemas, so I can use it
for schema in os.listdir(expParfilesDir) :
    schemaList.append(schema)
    parText = '''    
USERID=DBA_TS/dbats@UBSCS_LM
BUFFER=300000
FILE=struct_bscsdb.dmp
FROMUSER=''' + schema + '''
TOUSER=''' + schema + ''' 
STATISTICS=NONE
LOG=logs/''' + schema + '''.log
'''

    if not os.path.isdir(schemaParfilesDir) :
        os.makedirs(schemaParfilesDir)
        
    parfile=os.path.join(schemaParfilesDir,'imp_schema_'+ schema + '.par')
    
    with open(parfile, 'w') as f :
        f.write(parText)
        print "Writing " + parfile
        
####################################
#Scripts
####################################


if not os.path.isdir(scriptsDir) :
    os.makedirs(scriptsDir)
        
        
with open(tablespaceFile) as f:
    for line in f :
        tablespaceList.append(line.rstrip())
#Tablespace Quota Script        
for tablespaceName in tablespaceList :
    for schema in schemaList :
        scriptLine='alter user ' + schema + ' quota unlimited on ' + tablespaceName + ';'
        tablespaceQuotaScript.append(scriptLine)
        
with open(tablespaceQuotaScriptFile, 'w') as f :
    for line in tablespaceQuotaScript :
        f.write(line)
        f.write('\n')
print "Writing " + tablespaceQuotaScriptFile
    
        
print "Done!"    
    